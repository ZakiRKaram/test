//
//  RegularsViewController.swift
//  FinancialSecretary
//
//  Created by ZAKI AL-KARAM on 7/19/16.
//  Copyright © 2016 Zaki Al-Karam. All rights reserved.
//

import UIKit
import GRDB

class RegularsViewController: UITableViewController{
    @IBOutlet var totalTextLbl: UILabel!
    @IBOutlet var descBtn: UIButton!
    @IBOutlet var amountBtn: UIButton!
    @IBOutlet var dateBtn: UIButton!
    @IBOutlet var totalLbl: UILabel!
    @IBOutlet var img1: UIImageView!
    @IBOutlet var img2: UIImageView!
    @IBOutlet var img3: UIImageView!
    
    let defaults=UserDefaults.standard
    let nf=NumberFormatter()
    var rSpends: [Regular]!
    var regular: Regular!
    var orderBy: String!
    
    
    // MARK: - Loading View
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeStoryboard()
        nf.numberStyle = .currency
        nf.currencySymbol=""
        orderBy = defaults.string(forKey: "regularsOrderBy") ?? ""
        if orderBy == "" {
            orderBy = "order by lastPaid"
            defaults.set(orderBy, forKey: "regularssOrderBy")
            defaults.synchronize()
        }
    }
    
    func localizeStoryboard() {
        self.navigationItem.title = NSLocalizedString("Regular Spends", comment: "")
        totalTextLbl.text = NSLocalizedString("Total:", comment: "")
        descBtn.setTitle(NSLocalizedString("Description", comment: ""), for: UIControlState())
        amountBtn.setTitle(NSLocalizedString("Amount", comment: ""), for: UIControlState())
        dateBtn.setTitle(NSLocalizedString("Last Paid", comment: ""), for: UIControlState())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getRegulars()
        var totalRTransctions = NSDecimalNumber.zero
        for regular in rSpends {
            totalRTransctions = totalRTransctions.adding(regular.amount)
        }
        totalLbl.text = nf.string(from: totalRTransctions)
        setSortingImg()
        tableView.reloadData()
    }
    
    func getRegulars(){
        dbQueue.inDatabase{ db in
            do{
                rSpends = try Regular.fetchAll(db, "select r.*,  MAX(s.date) as lastPaid from regulars as r left join spends as s on r.id= s.regularId group by r.id \(orderBy!)")
            }catch{
                showErrorAlert(errorNumber: "28", controller: self)
            }
        }
    }
    
    // MARK: - Table View Data Source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rSpends.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "regularCell", for: indexPath) as! RegularCell
        let rSpend = rSpends[indexPath.row]
        cell.desc.text = rSpend.desc
        cell.amount.text = nf.string(from: rSpend.amount)
        cell.date.text = rSpend.lastPaid == nil ? NSLocalizedString("Not Paid", comment: "") : convertDatabaseDateToUser(rSpend.lastPaid!)
        return cell
    }
    
    
    // MARK: - Table View Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        regular = rSpends[indexPath.row]
        let view = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //necessary for iPad
        let cell = self.tableView.cellForRow(at: indexPath)!
        view.popoverPresentationController?.sourceView = cell
        view.popoverPresentationController?.sourceRect = cell.bounds
        
        let pay = UIAlertAction(title: NSLocalizedString("Pay", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
            self.performSegue(withIdentifier: "PayRTransction", sender: self)
        })
        let details = UIAlertAction(title: NSLocalizedString("Payments Details", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
            self.performSegue(withIdentifier: "ToPaymentsDetails", sender: self)
        })
        let edit = UIAlertAction(title: NSLocalizedString("Edit", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
            self.performSegue(withIdentifier: "EditRTransction", sender: self)
        })
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        view.addAction(pay)
        view.addAction(details)
        view.addAction(edit)
        view.addAction(cancel)
        present(view, animated: true, completion: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            regular = rSpends[indexPath.row]
            dbQueue.inDatabase{ db in
                do{
                    _ = try Regular.deleteOne(db, key: regular.id!)
                    try db.execute("delete from spends where regularId=\(regular.id!) and code='xr1-'")
                    
                    try db.execute("update spends set code='n1-' where regularId=\(regular.id!) and code='r1-'")
                    try db.execute("update spends set code='s1-' where regularId=\(regular.id!) and code='rs1-'")
                    try db.execute("update spends set code='du1-' where regularId=\(regular.id!) and code='rdu1-'")
                    try db.execute("update spends set code='xs1-' where regularId=\(regular.id!) and code='xrs1-'")
                    try db.execute("update spends set code='xdu1-' where regularId=\(regular.id!) and code='xrdu1-'")
                    
                    try db.execute("update spends set regularId=0 where regularId=\(regular.id!)")
                }catch{
                    showErrorAlert(errorNumber: "29", controller: self)
                }
            }

            self.viewWillAppear(true)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "PayRTransction") {
            let ftvc = segue.destination as! SpendForm
            ftvc.rSpend = regular
        }
        else if (segue.identifier == "ToPaymentsDetails") {
            let pdVC = segue.destination as! PaymentsDetailsTVController
            pdVC.regular = regular
        }
        else if (segue.identifier == "EditRTransction") {
            let rtv = segue.destination as! RegularForm
            rtv.regular = regular
        }
        
    }
    
    @IBAction func unwindToRegulars(_ segue: UIStoryboardSegue) {
    }
    
    
    // MARK: - IBAction
    @IBAction func sort(_ sender: AnyObject) {
        let btn: UIButton = (sender as! UIButton)
        if (btn.titleLabel!.text! == NSLocalizedString("Description", comment: "")){//"Description") || (btn.titleLabel!.text! == "الوصف") {
            if img1.image!==UIImage(named: "asc.png") {
                img1.image = UIImage(named: "desc.png")
                orderBy = "order by desc desc"
            }
            else {
                img1.image = UIImage(named: "asc.png")
                orderBy = "order by desc"
            }
            self.resetSortingImg(img1)
        }
        else if (btn.titleLabel!.text! == NSLocalizedString("Amount", comment: "")){//"Amount") || (btn.titleLabel!.text! == "المبلغ") {
            if img2.image!==UIImage(named: "asc.png") {
                img2.image = UIImage(named: "desc.png")
                orderBy = "order by amount desc"
            }
            else {
                img2.image = UIImage(named: "asc.png")
                orderBy = "order by amount"
            }
            self.resetSortingImg(img2)
        }
        else if (btn.titleLabel!.text! == NSLocalizedString("Last Paid", comment: "")) {// || (btn.titleLabel!.text! == "أخر دفع") {
            if img3.image!==UIImage(named: "asc.png") {
                img3.image = UIImage(named: "desc.png")
                orderBy = "order by lastPaid desc"
            }
            else {
                img3.image = UIImage(named: "asc.png")
                orderBy = "order by lastPaid"
            }
            self.resetSortingImg(img3)
        }
        
        defaults.set(orderBy!, forKey: "regularsOrderBy")
        defaults.synchronize()
        getRegulars()
        tableView.reloadData()
    }
    
    
    // MARK: - Helper Methods
    func resetSortingImg(_ img: UIImageView) {
        img1.isHidden = true
        img2.isHidden = true
        img3.isHidden = true
        img.isHidden = false
    }
    
    func setSortingImg() {
        if (orderBy == "order by desc desc") {
            img1.image = UIImage(named: "desc.png")
            self.resetSortingImg(img1)
        }
        else if (orderBy == "order by desc") {
            img1.image = UIImage(named: "asc.png")
            self.resetSortingImg(img1)
        }
        else if (orderBy == "order by amount desc") {
            img2.image = UIImage(named: "desc.png")
            self.resetSortingImg(img2)
        }
        else if (orderBy == "order by amount") {
            img2.image = UIImage(named: "asc.png")
            self.resetSortingImg(img2)
        }
        else if (orderBy == "order by lastPaid desc") {
            img3.image = UIImage(named: "desc.png")
            self.resetSortingImg(img3)
        }
        else if (orderBy == "order by lastPaid") {
            img3.image = UIImage(named: "asc.png")
            self.resetSortingImg(img3)
        }
    }
}
